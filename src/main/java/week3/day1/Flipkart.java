package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Flipkart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		
		driver.findElementByClassName("LM6RPg").sendKeys("Iphone X");
		driver.findElementByLinkText("Apple iPhone X (Silver, 256 GB)").click();
		
		Set<String> handles = driver.getWindowHandles();
		List<String> lst= new ArrayList<>();
		lst.addAll(handles);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		
		
		 }

}
