package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub


		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		
		Thread.sleep(3000);
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		
		List<WebElement> allrow = table.findElements(By.tagName("tr"));
		String text = table.getText();
		System.out.println(text); 
		
		System.out.println(allrow.size());//tells total rows
		
		WebElement particularrow = allrow.get(2);//tells the particular data in rows
		
		System.out.println(particularrow);
		
		List<WebElement> coloumn = particularrow.findElements(By.tagName("td"));
		
		System.out.println(coloumn.size());//shows all data in coloumns
		
		WebElement particularcoloumn = coloumn.get(2);
		System.out.println(particularcoloumn);
		
		String name=driver.findElementById("h1a").getText(); 
		
		System.out.println(name);
		
		if(name.equals(name)) {
			System.out.println("True");
		}else {
			System.out.println("False");
			}
			
		}

		
		
		
		
		

	

}
