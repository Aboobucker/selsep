package week1.day1;

import java.util.Scanner;

public class Pattern {
	
	public static void main(String[] args) {
		try {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number");
		int a = sc.nextInt();
		System.out.println("enter the last number");
		int b = sc.nextInt();
		sc.close();
		for (int i = a; i <= b; i++) {
			if (i % 3 == 0) {
				System.out.print("FIZZ");
				if (i % 5 == 0)
					System.out.print("BUZZ");
				System.out.print(" ");
			} else if (i % 5 == 0)
				System.out.print("BUZZ ");
			else
				System.out.print(i + " ");
		}}catch(ArithmeticException e) {
			System.out.println("Please enter valid inputs");
			
		}
	}

}
