package week1.day1;

public class SmartTv extends Television {
	
	public void wifi()
	{
		System.out.println("get access");
	}
	
	public void gestures()
	{
		System.out.println("change the channel");
	}

	public static void main(String[] args) {
		
		Entertainment obj=new Television();
		
		obj.channel1();
		obj.channel2();
		obj.channel3();
		
		System.out.println("smart tv objects");
		
		SmartTv obj1=new SmartTv();
		
		obj1.wifi();
		obj1.gestures();
		
	}
}
