package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods {
	@BeforeTest
	public void report() {
		testCaseName="ZoomCar";
		testDesc="Zoomcar application";
		author="abu";
		category="sanity testing"; 
		}
	@Test
	public void journey() {

		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement element1 = locateElement ("xpath","//a[@class='search']");
		click(element1);
		WebElement element2 = locateElement("xpath","//div[@class='items']");
		click(element2);
		WebElement element3 = locateElement("class","proceed");
		click(element3);

		//Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);

		WebElement element4 = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]");
		click(element4);

		WebElement element5 = locateElement("class","proceed");
		click(element5);

		WebElement element6 = locateElement("class","proceed");
		click(element6);

		List<WebElement> locateElements = locateElements("xpath","//div[@class='component-car-item']");
		int size = locateElements.size();
		System.out.println("the size is:-"+size);

		List<Integer> ls=new ArrayList<>();

		List<WebElement> locateElements2 = locateElements("xpath","//div[@class='price']");


		for(WebElement ch:locateElements2)
		{
			String text = ch.getText();
			String all = text.replaceAll("\\D","");
			System.out.println(all);
			
			int i=Integer.parseInt(all);
			ls.add(i);
			System.out.println("the replaced price are converted to integer"+i);

		}
		
		Collections.sort(ls);
		System.out.println(ls);
		
		int maxprice=ls.get(ls.size()-1);
		
		System.out.println(maxprice);
		
		 WebElement maxcar = locateElement("xpath","//div[contains(text(),'"+maxprice+"')]/../../..//following::div/h3");
		 
		 String text = maxcar.getText();
		 System.out.println(text);
		 
		 
		 
		 
		 
		 
		 
		}



}
