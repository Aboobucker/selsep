package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandling {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.findElementByLinkText("Contact Us").click();
		
	    Set<String> handles = driver.getWindowHandles();
	   
	   List<String> lst= new ArrayList<>();
	   lst.addAll(handles);//addall will add all the things from set.
	   driver.switchTo().window(lst.get(1));
	   
	   System.out.println(driver.getTitle());
	   
	   File screenshot = driver.getScreenshotAs(OutputType.FILE);
	   File obj=new File("./snaps/img1.png");
	   FileUtils.copyDirectory(screenshot, obj);
	   

	}

}
