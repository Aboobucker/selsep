package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alertsframes {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		driver.switchTo().frame("iframeResult");

		driver.findElementByXPath("//button[text()='Try it']").click();
		Thread.sleep(3000);

		Alert alert = driver.switchTo().alert();

		alert.sendKeys("Hello abu");
		Thread.sleep(3000);
		alert.accept();
		
		String text = driver.findElementById("demo").getText();
		
		if(text.contains("Hello world")) {
			System.out.println("text is correct");}
			else {
				System.out.println("text is incorrect");	
			}
			
			driver.switchTo().defaultContent();  
		}




	}


