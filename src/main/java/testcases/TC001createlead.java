package testcases;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.Projectmethods;
import week5.day2.Readexcel;

public class TC001createlead extends Projectmethods {

	//@Test(dependsOnMethods= {"testcases.TC001createlead.mergerlead"},alwaysRun=true )
	//@Test(invocationCount=2)
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="TC001createlead";
		testDesc="creating lead";
		category="smoke";
		author="abu";
		
	}
	//@Test(groups="smoke")
	@Test(dataProvider="qa")
	public void createlead(String cname, String fname, String lname) {

		WebElement element = locateElement("LinkText","Create Lead");
		click(element);
		WebElement element2 = locateElement("id","createLeadForm_companyName");
		type(element2,cname);
		WebElement element3 = locateElement("id","createLeadForm_firstName");
		type(element3,fname);
		WebElement element4 = locateElement("id","createLeadForm_lastName");
		type(element4,lname);
		WebElement element5 = locateElement("class","smallSubmit");
		element5.click();
		
		}
	
	@DataProvider(name="qa",indices= {1})
	public Object[][] fetchData() throws IOException{
		
		Object[][] data=Readexcel.readData();
		return data;
	/*Object[][] data=new Object[2][3];
	
data[0][0]="hcl";
data[0][1]="abu";
data[0][2]="l";

data[1][0]="tcs";
data[1][1]="ram";
data[1][2]="k";
return data;*/

}}


